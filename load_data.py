# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 09:43:18 2023

@author: Matthias Maldet
"""

import pandas as pd
import pyomo.core as pyomo
import numpy as np
import os





def load_scenario(filename):
    # Create an ExcelFile object
    df = pd.read_excel(filename, index_col=0, sheet_name="Scenario")
    return df  

def load_data(filename, test):
    data_struct={}
    # Create an ExcelFile object
    xls = pd.ExcelFile(filename)
    
    # Get the sheet names
    sheet_names = xls.sheet_names
    
    for sheet in sheet_names:
        
        if not ("consumer" in sheet.lower()):
            continue
        
        sheet_struct={}
        if(test):
            data=xls.parse(sheet, nrows=1)
        else:
            data=xls.parse(sheet)
        
        columns=data.columns
        
        sheet_struct[columns[1]] = data[columns[1]].values
        sheet_struct[columns[2]] = data[columns[2]].values[0]
        sheet_struct[columns[3]] = data[columns[3]].values
        sheet_struct[columns[4]] = data[columns[4]].values[0]
        sheet_struct[columns[5]] = data[columns[5]].values[0]
        sheet_struct[columns[6]] = data[columns[6]].values
        sheet_struct[columns[7]] = data[columns[7]].values[0]
        sheet_struct[columns[8]] = data[columns[8]].values
        sheet_struct[columns[9]] = data[columns[9]].values[0]
        sheet_struct[columns[10]] = data[columns[10]].values[0]
        sheet_struct[columns[11]] = data[columns[11]].values[0]
        sheet_struct[columns[12]] = data[columns[12]].values[0]
        sheet_struct[columns[13]] = data[columns[13]].values[0]
        sheet_struct[columns[14]] = data[columns[14]].values
        sheet_struct[columns[15]] = data[columns[15]].values[0]
        sheet_struct[columns[16]] = data[columns[16]].values
        sheet_struct[columns[17]] = data[columns[17]].values[0]
        sheet_struct[columns[18]] = data[columns[18]].values[0]
        sheet_struct[columns[19]] = data[columns[19]].values
        sheet_struct[columns[20]] = data[columns[20]].values[0]
        sheet_struct[columns[21]] = data[columns[21]].values[0]
        sheet_struct[columns[22]] = data[columns[22]].values[0]
        sheet_struct[columns[23]] = data[columns[23]].values[0]
        sheet_struct[columns[24]] = data[columns[24]].values[0]
        sheet_struct[columns[25]] = data[columns[25]].values
        sheet_struct[columns[26]] = data[columns[26]].values[0]
        sheet_struct[columns[27]] = data[columns[27]].values
        sheet_struct[columns[28]] = data[columns[28]].values[0]
        sheet_struct[columns[29]] = data[columns[29]].values[0]
        sheet_struct[columns[30]] = data[columns[30]].values
        sheet_struct[columns[31]] = data[columns[31]].values
        sheet_struct[columns[32]] = data[columns[32]].values
        sheet_struct[columns[33]] = data[columns[33]].values
        sheet_struct[columns[34]] = data[columns[34]].values
        sheet_struct[columns[35]] = data[columns[35]].values
        sheet_struct[columns[36]] = data[columns[36]].values[0]
        sheet_struct[columns[37]] = data[columns[37]].values
        sheet_struct[columns[38]] = data[columns[38]].values[0]
        sheet_struct[columns[39]] = data[columns[39]].values
        sheet_struct[columns[40]] = data[columns[40]].values[0]
        
        data_struct[sheet] = sheet_struct
        
        
    return data_struct


def load_results(m):
    model_vars = m.component_map(ctype=pyomo.Var)


    serieses = []   # collection to hold the converted "serieses"
    for k in model_vars.keys():   # this is a map of {name:pyo.Var}
    
        
        if("p_max" in str(k)):
            continue
        
        if("q_c2L" in str(k)):
            continue
        
        if("q_L2c" in str(k)):
            continue
        
        v = model_vars[k]
        
    
        # make a pd.Series from each    
        s = pd.Series(v.extract_values(), index=v.extract_values().keys())
    
        # if the series is multi-indexed we need to unstack it...
        if type(s.index[0]) == tuple:  # it is multi-indexed
            s = s.unstack(level=1)
        else:
            s = pd.DataFrame(s)         # force transition from Series -> df
        #print(s)
    
        # multi-index the columns
        s.columns = pd.MultiIndex.from_tuples([(k, t) for t in s.columns])
    
        serieses.append(s)
    
    df = pd.concat(serieses, axis=1)
    return df

def load_results_peakpower(m):
    model_vars = m.component_map(ctype=pyomo.Var)


    serieses = []   # collection to hold the converted "serieses"
    for k in model_vars.keys():   # this is a map of {name:pyo.Var}
        
        if not("p_max" in str(k)):
            continue
        v = model_vars[k]
        
    
        # make a pd.Series from each    
        s = pd.Series(v.extract_values(), index=v.extract_values().keys())
    
        # if the series is multi-indexed we need to unstack it...
        if type(s.index[0]) == tuple:  # it is multi-indexed
            s = s.unstack(level=1)
        else:
            s = pd.DataFrame(s)         # force transition from Series -> df
        #print(s)
    
        # multi-index the columns
        s.columns = pd.MultiIndex.from_tuples([(k, t) for t in s.columns])
    
        serieses.append(s)
    
    df = pd.concat(serieses, axis=1)
    return df

def load_results_trading(m):
    model_vars = m.component_map(ctype=pyomo.Var)


    serieses = []   # collection to hold the converted "serieses"
    for k in model_vars.keys():   # this is a map of {name:pyo.Var}
        
        if not("q_c2L" in str(k) or "q_L2c" in str(k)):
            continue
        v = model_vars[k]
        
    
        # make a pd.Series from each    
        s = pd.Series(v.extract_values(), index=v.extract_values().keys())
    
        # if the series is multi-indexed we need to unstack it...
        if type(s.index[0]) == tuple:  # it is multi-indexed
            s = s.unstack(level=1)
        else:
            s = pd.DataFrame(s)         # force transition from Series -> df
        #print(s)
    
        # multi-index the columns
        s.columns = pd.MultiIndex.from_tuples([(k, t) for t in s.columns])
    
        serieses.append(s)
    
    df = pd.concat(serieses, axis=1)
    return df
                
def load_data_EEG(filename):
    # Create an ExcelFile object
    df = pd.read_excel(filename, index_col=0, sheet_name="EEG")
    return df  

def sort_EEG(data, consumers):
    # Create an ExcelFile object
    
    #Empty dataframe
    df_total = pd.DataFrame()
    
    for consumer in consumers:
        results1 = data[("q_c2L", consumer)]
        
        for consumer1 in  consumers:
            results2 = results1.xs(consumer1, level=1).values
            #heading= "q_c2L_" + consumer.split('_')[1] + consumer1.split('_')[1]
            heading= ("q_c2L", consumer, consumer1)
            #results3 = np.insert(results2.astype(str), 0, heading)
            
            df_total[heading] = results2
            
    for consumer in consumers:
        results1 = data[("q_L2c", consumer)]
        
        for consumer1 in  consumers:
            results2 = results1.xs(consumer1, level=1).values
            #heading= "q_L2c_" + consumer.split('_')[1] + consumer1.split('_')[1]
            heading= ("q_L2c", consumer, consumer1)
            
            #results3 = np.insert(results2.astype(str), 0, heading)
            
            df_total[heading] = results2
            
            
    return df_total

def data_to_excel(scenario, filename, df, energy_community, df_costs):
    if not os.path.exists(scenario):
        os.makedirs(scenario)
        
    file_path = scenario + '/' + filename

    with pd.ExcelWriter(file_path, engine='xlsxwriter') as writer:
        sheet_name = 'Timeseries'
        df[0].to_excel(writer, sheet_name=sheet_name, index=True)
        sheet_name = 'PeakPower'
        df[1].to_excel(writer, sheet_name=sheet_name, index=True)
        if energy_community:
            sheet_name = 'Community'
            df[2].to_excel(writer, sheet_name=sheet_name, index=True)
         
        sheet_name = 'Objective'
        df_costs[0].to_excel(writer, sheet_name=sheet_name, index=True)
        sheet_name = 'Costs_Timeseries'
        df_costs[1].to_excel(writer, sheet_name=sheet_name, index=True)
        sheet_name = 'Costs_PeakPower'
        df_costs[2].to_excel(writer, sheet_name=sheet_name, index=True)
        if energy_community:
            sheet_name = 'Costs_Community'
            df_costs[3].to_excel(writer, sheet_name=sheet_name, index=True)  
        
     
    string = "Data written"
    return string


def get_costs_timeseries(df, input_data, consumers):
    df_new = pd.DataFrame()
    
    for consumer in consumers:
        df_new[("q_elgrid", consumer)]= df["q_elgrid"][consumer].values*(input_data[consumer]["C_elenergy"]+input_data[consumer]["C_elgrid"])
    
    for consumer in consumers:
        df_new[("q_feedin", consumer)]= df["q_feedin"][consumer].values*(-input_data[consumer]["C_Feedin"])
        
    for consumer in consumers:
        df_new[("q_eldemand", consumer)]= df["q_eldemand"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_pv", consumer)]= df["q_pv"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_batcharge", consumer)]= df["q_batcharge"][consumer].values*(input_data[consumer]["C_bat"])
        
    for consumer in consumers:
        df_new[("q_batdischarge", consumer)]= df["q_batdischarge"][consumer].values*(input_data[consumer]["C_bat"])
        
    for consumer in consumers:
        df_new[("q_batSOC", consumer)]= df["q_batSOC"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_heatdemand", consumer)]= df["q_heatdemand"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_HPel", consumer)]= df["q_HPel"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_HPth", consumer)]= df["q_HPth"][consumer].values*(input_data[consumer]["C_hp"])
        
    for consumer in consumers:
        df_new[("q_dh", consumer)]= df["q_dh"][consumer].values*(input_data[consumer]["C_districtheat"])
        
    for consumer in consumers:
        df_new[("q_thstorecharge", consumer)]= df["q_thstorecharge"][consumer].values*(input_data[consumer]["C_thstore"])
        
    for consumer in consumers:
        df_new[("q_thstoredischarge", consumer)]= df["q_thstoredischarge"][consumer].values*(input_data[consumer]["C_thstore"])
        
    for consumer in consumers:
        df_new[("q_thstoreSOC", consumer)]= df["q_thstoreSOC"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_cooldemand", consumer)]= df["q_cooldemand"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_elCoolel", consumer)]= df["q_elCoolel"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_elCoolcool", consumer)]= df["q_elCoolcool"][consumer].values*(input_data[consumer]["C_elcool"])
        
    for consumer in consumers:
        df_new[("q_dcool", consumer)]= df["q_dcool"][consumer].values*(input_data[consumer]["C_districtcool"])
        
    for consumer in consumers:
        df_new[("em_elgrid", consumer)]= df["em_elgrid"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("em_dh", consumer)]= df["em_dh"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("em_dcool", consumer)]= df["em_dcool"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("em_total", consumer)]= df["em_total"][consumer].values*(input_data[consumer]["P_CO2"])
        
    return df_new


def get_costs_peakpower(df, input_data, consumers, aggregated):
    df_new = pd.DataFrame()
    
    if(aggregated):
        
        for consumer in consumers:
            df_new[("p_max", consumer)]= [0]
        
        df_new[("p_max_agg", "aggregated")]=df["p_max_agg"].values[-1]*input_data[consumers[0]]["C_elpower"]
        
    else:
    
        for consumer in consumers:
            df_new[("p_max", consumer)]= df["p_max"].loc[consumer]*input_data[consumer]["C_elpower"]
            
        df_new[("p_max_agg", "aggregated")]=[0]
    
    
        
    return df_new


def get_costs_EEG(df, input_data, consumers, input_data_EEG):
    
    df_new = pd.DataFrame()
    
    for consumer in consumers:
        for consumer1 in consumers:
            if(consumer != consumer1):
                df_new[("q_c2L", consumer, consumer1)] = df[("q_c2L", consumer, consumer1)]*((input_data[consumer]["C_elenergy"]+input_data[consumer]["C_Feedin"])/2+input_data[consumer]["C_elgrid"]*input_data_EEG[consumer][consumer1])
            
      
    for consumer in consumers:
        for consumer1 in consumers:
            if(consumer != consumer1):
                df_new[("q_L2c", consumer, consumer1)] = df[("q_L2c", consumer, consumer1)]*(-(input_data[consumer]["C_elenergy"]+input_data[consumer]["C_Feedin"])/2)  
      
    return df_new
        
    
    